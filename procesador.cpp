/*  
    ./procesador 2 "binario.bin" 0/1
    0 : Modo normal
    1 : Modo debugging
*/
#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;

int AC = 0;

void Ejecutar(int instr, int mode){
    int oper, data;
    oper = (0xff000000 & instr) >> 24;
    data =  0x00ffffff & instr;

    if(data & 0x00800000)               //Significa que data es negativo
        data = data + 0xff000000;

    switch(oper){
        case 1:                         //LOAD
            if(mode) cout << "Cargo el valor " << dec << data << " en AC" << endl;
            AC = data;
        break;

        case 2:                         //ADD
            if(mode) cout << "Adiciono el valor " << dec << data << " en AC" << endl;
            AC += data;
        break;

        case 3:                         //PRINT
             cout << "AC = " << dec << AC << endl;
        break;

        default:
            break;
    }
}

int main(int n, char * argv[]){
    char byte = 0;
    int instr = 0, aux = 0;
    ifstream bin_In;
    
    int mode = stoi(argv[3]);

    bin_In.open(argv[2], ios_base::binary);

    if(!bin_In.is_open()){
        cout << "Error al abrir el archivo binario" << endl;
        exit(-1);
    }

    if(mode)cout << "Modo debugging"<< endl;
    else    cout << "Modo normal"   << endl;

    cout << endl;

    bin_In.seekg (0, bin_In.end);
    int length = bin_In.tellg();        // Guardo la longitud del archivo binario,
    bin_In.seekg (0, bin_In.beg);

    int numByte = 0;
    
    while(bin_In.tellg() != length){
        instr = 0;
        for(int j = 0; j < 4; j++){     // Leo 4 bytes y lo almaceno en instr.
            bin_In.seekg (4*numByte + j, bin_In.beg);
            bin_In.get(byte);

            aux = (unsigned char)byte;
            instr = instr + (aux << 8*j);
        }
        if(mode) cout << numByte + 1 << "| "; // Numero de linea en modo debugging.
        Ejecutar(instr, mode);
        numByte++;
    }

    bin_In.close();
    return 0;
}
