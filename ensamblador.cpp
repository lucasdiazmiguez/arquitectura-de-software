#include <iostream>
#include <fstream>
#include <string.h>
#include <bitset>
#define N 24
#define LOAD "LOAD"
#define ADD "ADD"
#define PRINT "PRINT"
using namespace std;

int devolverPosicion(char chain[], char a)
{
    char *indice;  //a es L o A o P
    for (size_t i = 0; i < strlen(chain); i++)
        if ((a) == (chain[i]))
            return i; // retorno el indice en donde encuentra el comienzo de la palabra (LOAD,PRINT O ADD)
    return 0;
}

void copiarCadena(char cadenaOld[], int posicion, char cadenaNew[])
{

    for (size_t i = 0; i < strlen(cadenaOld) - posicion; i++)
        cadenaNew[i] = cadenaOld[posicion + i];
}
int noNumbersLeft(char string[], int desde)
{

    while (desde < 100) //desde el ultimo valor que agregué, avanzo en cadena
    {
        if (!(string[(desde)] == ' ' || string[(desde)] == '\r' || string[(desde)] == '\0' || string[(desde)] == '0'))
        {
            cout << desde << endl;
            cout << " algo " << string[(desde)] << "<=== exit 1" << endl;
            return 0;
        }
        desde++;
    }
    return 1;
}
int spaceNext(char cadena[], int desde, char aux[]) //recibe como parametros la cadena, el aux y el valor desde donde termina la instruccion(caso load 4 caso add 3)
{

    int j, i = 0, ultimoNumero = 0, ingresoLetra = 0, largo;
    int num = 0;
    int rangoSuperior = 8388608 - 1; // 2^(24-1) - 1;
    int rangoInferior = -8388608;    //-2^(24-1);
    if (cadena[desde] == ' ')
    {

        i = desde;
        while ((cadena[(i)] == ' ' || cadena[(i)] == '\r' || cadena[(i)] == '\0' || cadena[(i)] == '0') && i < 95) // el buscador de numeros
            i++;                                                                                                   // aca encuentra el valor, o sea los operandos

        j = 0;
        while (cadena[i] != '\0' && cadena[i] != ' ' && cadena[i] != '\r')
            aux[j++] = cadena[i++]; //copia en aux desde los primeros valores(tienen que ser numeros)
        //va copiando  los valores en aux y en cadena

        //caso en el que cadena no puso ningun, y pone un espacio despues de la  instruccion
        if (j == 0)
            aux[j] = '0';
        else
            aux[j] = '\0';

        largo = j;
        ultimoNumero = i;
        if (!noNumbersLeft(cadena, ultimoNumero)) //calcula desde la posicion i si hay mas caracteres, si hay retorna 0, sino retorna 1
            return 0;// sin esta funcion, el programa anda pero solo toma el primer operando, ej si pone ADD 30   50 solo toma el 30

        for (size_t i = ultimoNumero; i < strlen(aux); i++)
            aux[i] = {'\0'};// rellena con \0 los demas espacios del vector que no sirven
            //TODO  creo que puedo crear otro vector, con tamano 'ultimo numero' y le paso aux hasta ultimo numero asi me ahorro este paso, no se como lo ves

        for (size_t i = 0; i < largo; i++)
        {
            if (!(aux[i] == ' ' || aux[i] == '-' || aux[i] == '\r' || aux[i] == '\0' || (isdigit(aux[i])))) //este if dice, si no es un espacio, menos,etc, que pase, es para ver si es letra
            {
                cout << "no es un asdfsdf, aux[" << i << "]= " << aux[i] << " asique malo" << endl;
                ingresoLetra = 1;
                cout << "ingreso una letra en los operandos" << endl;
                return 0;
            }
        }
        //si hay letra que salga de la funcion retornando 0

        num = stoi(aux);
        if (num > (rangoSuperior) || num < (rangoInferior))
        {
            //corroboro que este en el rango, si no esta en rango retorna 0
            cout << "numero demasiado grande o chico, exit-1" << endl;
            return 0;
        }

        return 1;
    }
    else if (cadena[desde] == '\0' || cadena[desde] == '\r')//TODO este else if
    {
        return 1;//retorna 1 para el programa siga ejecutando por mas que haya una entrada de linea o en el caso de que el usuario ponga ADD\r
    }
    else
    {
        cout << "Ingrese un espacio entre la instruccion y los operandos" << endl;
        return 0;
    }
}

void agregoValoresAlFile(int salida, int num, ofstream &out)
{
    salida = (salida << 24);
    salida = salida | (0x00ffffff & num);
    cout << "Soy salida final en binario--> " << bitset<32>{(unsigned int)salida} << endl;
    out.write((const char *)&salida, sizeof(salida));
    cout << "---------------" << endl;
}
int main(int n, char *argv[])
{
    char cadena[100], aux[100], newChain[100], posicion = 0;

    int salida = 0xffffffff;
    int lineadeLectura = 1;
    ifstream archivoLectura;
    int todoCorrecto = 1;
    int i, j = 0, num;

    archivoLectura.open(argv[1]);
    ofstream out("binario.bin", ios::out | ios::binary | ios::trunc);
    out.is_open();
    while (!archivoLectura.eof() && todoCorrecto){
        for (size_t i = 0; i < 100; i++)
            aux[i] = cadena[i] = newChain[i] = {' '}; //inicializo en 0 cadena porque sino tomaba  valores random

        archivoLectura.getline(cadena, sizeof(cadena)); //tomo la primer linea
        num = 0, salida = 0;

        if (strstr(cadena, "LOAD") != NULL){
            cout << "LOAD" << endl;
            posicion = devolverPosicion(cadena, 'L'); // estas funciones
            copiarCadena(cadena, posicion, newChain);    // son para que si el usuario pone espacios adelante, se cree una copia del vector que empieza con la palabra y los operandos.
            salida = 0x00000001;
            if (spaceNext(newChain, 4, aux)){ //esta funcion devuelve el vector aux con los operandos y verifica que este correcto toda la linea.
                num = stoi(aux);
                agregoValoresAlFile(salida, num, out); //agrega valores al file si esta todo ok
            }
            else{
                cout << "sintax error in line: " << lineadeLectura << endl;
                todoCorrecto = 0;
            }
        }
        else if(strstr(cadena, "ADD") != NULL){
            cout << "ADD" << endl;
            posicion = devolverPosicion(cadena, 'A');
            copiarCadena(cadena, posicion, newChain);
            salida = 0x00000002;
            if (spaceNext(newChain, 3, aux)){
                num = stoi(aux);
                agregoValoresAlFile(salida, num, out);
            }
            else{
                cout << "sintax error in line: " << lineadeLectura << endl;
                todoCorrecto = 0;
            }
        }
        else if(strstr(cadena, "PRINT") != NULL){
            cout << "PRINT" << endl;
            posicion = devolverPosicion(cadena, 'P');
            copiarCadena(cadena, posicion, newChain);
            salida = 0x00000003;
            if (!noNumbersLeft(newChain, 5)){
                cout << "sintax error in line: " << lineadeLectura << endl;
                todoCorrecto = 0;
            }
            else{
                num = 0;
                agregoValoresAlFile(salida, num, out);
            }
        }

        if(!todoCorrecto){
            cout << "revise el archivo assembler, exit -1" << endl;
            return 0;
        }
        lineadeLectura++;
    }

    out.close();

    archivoLectura.close();
    return 0;
}